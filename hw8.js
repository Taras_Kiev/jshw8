"use strict";

// Теоретичні питання

// 1 - Це фактично все що є на сторінці у вигляді частин які ми можемо змінювати співпрацюючи з ними.
// 2 - innerHTML повертає внутрішній HTML елемента
// innerText повертає сам текст
// 3 - Є декілька варіантів, але найсучаснійший - document.querySelector("")


// Завдання практичне
// 1.
//a.
// let elements = document.querySelectorAll('p');
// for (let i = 0; i < elements.length; i++) {
//     elements[i].style.backgroundColor = "#ff0000";
// }
//b.
let elem = document.getElementsByTagName('p');
for (let i = 0; i < elem.length; i++) {
    elem[i].style.backgroundColor = "#ff0000";
}
// 2.
console.log(document.getElementById('optionsList'));
console.log(document.getElementById('optionsList').parentElement);
console.log(document.getElementById('optionsList').parentNode);

//3.
let root = document.getElementById('testParagraph');
root.textContent = "This is a paragraph";

//4.
let main = document.querySelectorAll('.main-header');
main[0].className = main[0].className + " " + "nav-item";
// main.classList.add('nav-item');
console.log(main);

//5.
let del = document.getElementsByClassName("section-title");
del[1].remove("section-title");
del[0].remove("section-title");
console.log(del);


